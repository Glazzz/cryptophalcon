<?php

class BinanceApi implements ExchangeApi
{
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $secretKey;

    protected $baseUri = 'https://api.binance.com/api/v3';

    public function __construct(string $apiKey, string $secretKey)
    {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
    }

    /**
     * @param string $url
     * @param array $params
     * @return array
     */
    protected function signedRequest(string $url, $params = []): array
    {
        $headers[] = "User-Agent: Mozilla/4.0 (compatible; PHP Binance API)\r\nX-MBX-APIKEY: {$this->apiKey}\r\n";
        $params['timestamp'] = number_format(microtime(true) * 1000, 0, '.', '');
        $query = http_build_query($params, '', '&');
        $signature = hash_hmac('sha256', $query, $this->secretKey);
        $endpoint = "{$this->baseUri}{$url}?{$query}&signature={$signature}";
        return json_decode($this->httpRequest($endpoint, $headers), true);
    }

    /**
     * @param $url
     * @param $headers
     * @param array $data
     * @return bool|mixed
     */
    protected function httpRequest(string $url, array $headers, $data = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        if ($data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            $content = false;
        }
        curl_close($ch);
        return $content;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        $data = $this->signedRequest('/account');

        if (empty($data['balances'])) {
            return 0.00;
        }

        $btcValue = 0.00;

        foreach ($data['balances'] as $balance) {
            $btcValue += $balance['free'];
        }

        return $btcValue;
    }
}