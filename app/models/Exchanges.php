<?php

class Exchanges extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=50, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(column="url", type="string", length=50, nullable=false)
     */
    public $url;

    /**
     *
     * @var string
     * @Column(column="api_key", type="string", length=255, nullable=false)
     */
    public $api_key;

    /**
     *
     * @var string
     * @Column(column="secret_key", type="string", length=255, nullable=false)
     */
    public $secret_key;

    /**
     * @var float
     */
    public $balance = 0;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('cryptophalcon');
        $this->setSource('Exchanges');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Exchanges';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Exchanges[]|Exchanges|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Exchanges|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Get Balance from exchange.
     *
     * @return float
     */
    public function getBalance(): float
    {
        $moduleName = sprintf('%sApi', ucfirst($this->name));
        $modelFileName = sprintf('%s/%s.php', __DIR__, $moduleName);

        if (file_exists($modelFileName)) {
            /** @var ExchangeApi $api */
            $api = new $moduleName($this->api_key, $this->secret_key);

            $this->balance = $api->getBalance();
        }

        return $this->balance + rand(0, 10);
    }

}
