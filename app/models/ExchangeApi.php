<?php

interface ExchangeApi {

    public function __construct(string $apiKey, string $secretKey);

    public function getBalance();
}