<?php

use Phalcon\Http\Request;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $totalBtc = 0;
        $exchanges = Exchanges::find();

        // TODO: Cache the values with specific timelife (need to save in config)
        foreach ($exchanges as $key => $exchange) {
            $balance = $exchange->getBalance();
            $exchanges[$key]->balance = $balance;
            $totalBtc += $balance;
        }

        $this->view->exchanges = $exchanges;
        $this->view->totalBtc = $totalBtc;
    }

}

