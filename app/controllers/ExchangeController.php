<?php

use Phalcon\Http\Request;

class ExchangeController extends \Phalcon\Mvc\Controller
{
    /**
     * Showing add new exchange form
     */
    public function addAction()
    {

    }

    /**
     * Save new exchange
     */
    public function saveAction()
    {
        // Get POST data
        $name = $this->request->getPost('name');
        $url = $this->request->getPost('url');
        $apiKey = $this->request->getPost('api_key');
        $secretKey = $this->request->getPost('secret_key');

        // Save Data
        $exchange = new Exchanges();
        $exchange->name = $name;
        $exchange->url = $url;
        $exchange->api_key = $apiKey;
        $exchange->secret_key = $secretKey;
        $exchange->save();

        // Redirect to main page.
        $this->response->redirect('/');
        $this->view->disable();
    }

}

